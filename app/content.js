'use strict';

const getEl = (el) => {
  return document.querySelectorAll(el);
};

const removeEmptyStatements = (arr) => {
  return arr.filter(item => !!item);
};

const hash = (text) => {
  let hash = 0, i, chr;

  if (text.length === 0) return hash;

  for (i = 0; i < text.length; i++) {
    chr   = text.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }

  return hash;
}

const init = () => {
  const nodeList = getEl("body");
  const content = [];

  for(let i =0; i < nodeList.length; ++i) {
    if(nodeList[i].innerText) {
      content.push(nodeList[i].innerText);
    }
  }

  let filteredContent = content.map(c => c.split("\n"))

  // side effects
  filteredContent = filteredContent.map(c => removeEmptyStatements(c));
  filteredContent = [].concat(...filteredContent);

  const contentHash = filteredContent.map(c => {
    if(typeof c === "string") {
      return {
        [hash(c)]: c.split(" ")
      };
    }
  });

  console.log((contentHash));

  fetch("http://localhost:4000/tokens",{
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method:'POST',
    body:JSON.stringify({"hash":contentHash.slice(0, 200)})
  })
  .then((res)=>{
    return res.json();
  })
  .then(res => {
    console.log(res.data);

    const $h1 = document.querySelectorAll("h1");
    const $p = document.querySelectorAll("p");

    for(let i=0; i< $h1.length; ++i) {
      for(let j=0; j<res.data.length; ++j) {
        if($h1[i].innerText.indexOf(res.data[j]) !== -1) {
          $h1[i].style.color = "transparent";
          $h1[i].style.textShadow = "0 0 5px rgba(0,0,0,0.5)";
        }
      }
    }

    for(let i=0; i< $p.length; ++i) {
      for(let j=0; j<res.data.length; ++j) {
        if($p[i].innerText.indexOf(res.data[j]) !== -1) {
          const idx = $p[i].innerText.indexOf(res.data[j]);
          const text = $p[i].innerText.slice(0, idx) + `<span style='color: transparent; text-shadow: 0 0 10px rgba(0,0,0,0.5)'>${res.data[j]}</span>` + $p[i].innerText.slice(idx+res.data[j].length, $p[i].innerText.length);
          // $p[i].style.color = "transparent";
          // $p[i].style.textShadow = "0 0 5px rgba(0,0,0,0.5)";

          $p[i].innerHTML = text;
        }
      }
    }
  });
}


init();
