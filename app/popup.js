"use strict";

const sendMessage = () => {
  var activeTab;
  var activeTabQuery = { active: true, currentWindow: true };

  chrome.tabs.query(activeTabQuery, function(tabs) {
    activeTab = tabs[0];

    chrome.tabs.sendMessage(activeTab.id, {
      msg: "Msg.START_FILTER",
    });
  });
}

sendMessage();
