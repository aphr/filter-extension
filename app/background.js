'use strict';

// Use namspsce
chrome.runtime.onMessage.addListener(function(request, response, callback) {
  if(request.msg === 'Msg.START_FILTER') {
    console.log("Yay")
  }
});

// Set toggle state to false, when tab is reloaded or url changes
chrome.tabs.onUpdated.addListener(function(tabId, info, tab) {
  console.log(`${tabId} updated`)
});

// Set toggle state to false, when tab is closed
chrome.tabs.onRemoved.addListener(function(tabId, info) {
  console.log(`${tabId} removed`)
});
